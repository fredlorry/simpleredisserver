#pragma once

class ClientSocket;

#include "socket.h"

class ClientSocket: private Socket {
public:
    int get_fd() const {    return m_sock;    }
};