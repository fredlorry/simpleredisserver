#include "socket.h"

Socket::Socket(): m_sock(-1) {
    memset(&m_addr, 0, sizeof(m_addr));
}

Socket::~Socket() {}

bool Socket::create() {
    // Получаем дескриптор.
    m_sock = socket(PF_INET, SOCK_STREAM, 0);
    if (! is_valid())
        return false;
    // Выставляем флаг сокета SO_REUSEADDR в 1.
    int on = 1; // Значение флага.
    int setsockopt_ret = setsockopt(m_sock,
                                        SOL_SOCKET,
                                        SO_REUSEADDR,
                                        (const char*) &on,
                                        sizeof(on));
    return (setsockopt_ret != -1);
}

bool Socket::bind(const int port) {
    if (! is_valid())
        return false;
    // Выставляем информацию в структуру.
    m_addr.sin_family = AF_INET;
    m_addr.sin_addr.s_addr = INADDR_ANY;
    m_addr.sin_port = htons(port);
    // Привязываем порт к сокету/дескриптору.
    int bind_ret = ::bind(m_sock,
                            (struct sockaddr *) &m_addr,
                            sizeof(m_addr));
    return (bind_ret != -1);
}

bool Socket::listen() const {
    if (! is_valid())
        return false;
    
    int listen_ret = ::listen(m_sock, MAXCONNECTIONS);
    return (listen_ret != -1);
}

bool Socket::accept(Socket& client_socket) const {
    int addr_length = sizeof(m_addr);
    client_socket.m_sock = ::accept(m_sock, (sockaddr*) &m_addr, (socklen_t*) &addr_length);
    return (client_socket.m_sock > 0);
}

void Socket::close() {
    ::shutdown(m_sock, SHUT_RDWR);
    ::close(m_sock);
    m_sock = -1;
    memset(&m_addr, 0, sizeof(m_addr));
}

bool Socket::send(char* buf, size_t len) const {
    int send_ret = 1;
    while (send_ret > 0 && len > 0) {
        send_ret = ::send(m_sock, buf, len, MSG_NOSIGNAL);
        len -= send_ret;
        buf += send_ret;
    }
    if (send_ret == -1)
        throw std::runtime_error("Send went wrong.");
    return send_ret;
}

int Socket::recv(char* buf, size_t len) {
    memset(buf, 0, len);
    int recv_ret = ::recv(m_sock, buf, len, 0);
    if (recv_ret == 0) { // Соединение закрыто.
        throw std::runtime_error("Connection closed.");
        close();
    }
    if (recv_ret < 0) // Произошла ошибка.
        throw std::runtime_error("Recv execution went really bad.");
    return recv_ret;
}

void Socket::set_non_blocking(const bool b) {
    int opts;
    opts = fcntl(m_sock, F_GETFL);
    if (opts == -1)
        return;
    if (b)
        opts = (opts | O_NONBLOCK);
    else
        opts = (opts & ~O_NONBLOCK);
    fcntl(m_sock, F_SETFL, opts);
}
