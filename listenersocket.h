#pragma once

class ListenerSocket;

#include "socket.h"

class ListenerSocket: public Socket {
public:
    ListenerSocket(int port);
    ~ListenerSocket();
    bool accept(Socket&);

};
