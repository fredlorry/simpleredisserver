#include <netinet/in.h>
#include <fcntl.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

class SingletonProcess
{
public:
    SingletonProcess(uint16_t port0): socket_fd(-1), rc(1), port(port0) {}

    ~SingletonProcess() {   if (socket_fd != -1)    close(socket_fd);   }

    bool operator()() {
        if (socket_fd == -1 || rc) {
            socket_fd = -1;
            rc = 1;

            if ((socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
                throw std::runtime_error(std::string("Could not create socket: ") +  strerror(errno));
            } else {
                struct sockaddr_in name;
                name.sin_family = AF_INET;
                name.sin_port = htons (port);
                name.sin_addr.s_addr = htonl (INADDR_ANY);
                rc = bind (socket_fd, (struct sockaddr *) &name, sizeof (name));
            }
        }
        return (socket_fd != -1 && rc == 0);
    }

    std::string GetLockFileName() {
        return "Port " + std::to_string(port);
    }

private:
    int socket_fd;
    int rc;
    uint16_t port;
};

int main() {
    SingletonProcess singleton(5555);
    if (!singleton()) {
        std::cerr << "Process running already. See " << singleton.GetLockFileName() << std::endl;
    return 1;
    }
    std::cout << "Sleeping...ZzZzz~" << std::endl;
    sleep(20);
    std::cout << "Woke up." << std::endl;
    int fd = open("./2015.txt", O_WRONLY | O_CREAT | O_EXCL, 0222);
    if (fd > 0) {
        write(fd, "-1000000", 8);
        close(fd);
   }
}