#include "reader.h"

void Reader::clear() {
    rpos_ = end_;
}
char Reader::read_char() {
    if (rpos_ == end_) read_more();
    return buffer_[rpos_++];
}

std::string Reader::read_line() {
    char cur_char = read_char();
    std::string result;
    while (cur_char != '\r') {
        result.append(1, cur_char);
        cur_char = read_char();
    }
    read_char(); // skip "\n"
    return result;
}

std::vector<char> Reader::read_raw(size_t len) {
    std::vector<char> result(len);
    for (size_t i = 0; i != len; ++i)
        result[i] = read_char();
    read_char(); // skip "\r"
    read_char(); // skip "\n"
    return result;
}

int64_t Reader::read_int() {
    int64_t i = 0;
    bool negative = false;

    char first = read_char(), next;
    if (first == '-') {
        negative = true;
        next = read_char();
    } else {
        next = first;
    }

    do {
        i *= 10;
        i += next - '0';

        next = read_char();
    } while(next != '\r');
    read_char(); // skip '\n'

    return negative ? -i : i;
}

void StringReader::read_more() {
    if (input.empty())
        throw std::runtime_error("End of input.");

    end_ = 0;
    rpos_ = 0;
    for (; end_ < input.size() && end_ < buffer_.size(); ++end_) {
        buffer_[end_] = input[end_];
    }

    input.erase(input.begin(), input.begin() + end_);
}

void SocketReader::read_more() {
    rpos_ = 0;
    int act_recv = socket_ptr->recv(&buffer_[0], buffer_.size()); // Little hack.
    end_ = act_recv;
}
