#pragma once

class Cmd;
class SHUTDOWN;

#include "table.h"
#include "server.h"
#include "redis.h"

class Cmd {
public:
Cmd(Table* t_ptr, Server* s_ptr): table_ptr(t_ptr), server_ptr(s_ptr) {};
    virtual ~Cmd() {};

    virtual std::vector<char> get_name() = 0;
    virtual RedisValue exec(RedisValue& args) = 0;
protected:
    Server* server_ptr;
    Table* table_ptr;
private:
    const char* name;
};

class SHUTDOWN : public Cmd {
public:
SHUTDOWN(Table* t_ptr, Server* s_ptr);

    std::vector<char> get_name() override;
    RedisValue exec(RedisValue& args) override;
private:
    const char* name = "SHUTDOWN";
};

class GET : public Cmd {
public:
GET(Table* t_ptr, Server* s_ptr);

    std::vector<char> get_name() override;
    RedisValue exec(RedisValue& args) override;
private:
    const char* name = "GET";
};

class SET : public Cmd {
public:
SET(Table* t_ptr, Server* s_ptr);

    std::vector<char> get_name() override;
    RedisValue exec(RedisValue& args) override;
private:
    const char* name = "SET";
};

class DEL : public Cmd {
public:
DEL(Table* t_ptr, Server* s_ptr);

    std::vector<char> get_name() override;
    RedisValue exec(RedisValue& args) override;
private:
    const char* name = "DEL";
};