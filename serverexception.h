#pragma once

#include <string>
#include <exception>

class ServerException: public std::exception {
public:
    ServerException(const char* s): m_s(s) {};
    ~ServerException() {};
    const char* what() {    return m_s.c_str();    }
private:
    std::string m_s;
};