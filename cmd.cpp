#include "cmd.h"

SHUTDOWN::SHUTDOWN(Table* t_ptr, Server* s_ptr): Cmd(t_ptr, s_ptr) {}

std::vector<char> SHUTDOWN::get_name() {
    return construct_bstr(name);
}

RedisValue SHUTDOWN::exec(RedisValue& args) {
    std::cerr << name << " was called.\n";
    server_ptr->shutdown();
    return construct_bstr("Server shut down.");
}

SET::SET(Table* t_ptr, Server* s_ptr): Cmd(t_ptr, s_ptr) {}

std::vector<char> SET::get_name() {
    return construct_bstr(name);
}

RedisValue SET::exec(RedisValue& args) {
    std::cerr << name << " was called.\n";
    if (boost::get<std::vector<RedisValue>>(args).size() == 3)
    	if (!table_ptr->contains(boost::get<std::vector<char>>(boost::get<std::vector<RedisValue>>(args)[1]))) {
    		table_ptr->set(boost::get<std::vector<char>>(boost::get<std::vector<RedisValue>>(args)[1]), // key
    						boost::get<std::vector<RedisValue>>(args)[2]); // value
    		return construct_bstr("Succesfully set.");
    	} else {
    		return RedisError("Already set.");
    	}
    return RedisError("Invalid command use, usage: \"SET key value\" ");
}

GET::GET(Table* t_ptr, Server* s_ptr): Cmd(t_ptr, s_ptr) {}

std::vector<char> GET::get_name() {
    return construct_bstr(name);
}

RedisValue GET::exec(RedisValue& args) {
    std::cerr << name << " was called.\n";
    if (boost::get<std::vector<RedisValue>>(args).size() == 2)
    	if (table_ptr->contains(boost::get<std::vector<char>>(boost::get<std::vector<RedisValue>>(args)[1]))) {
    		return *table_ptr->get(boost::get<std::vector<char>>(boost::get<std::vector<RedisValue>>(args)[1]));
    	} else {
    		return RedisError("Value wasn't found.");
    	}
    return RedisError("Invalid command use, usage: \"GET key\" ");
}

DEL::DEL(Table* t_ptr, Server* s_ptr): Cmd(t_ptr, s_ptr) {}

std::vector<char> DEL::get_name() {
    return construct_bstr(name);
}

RedisValue DEL::exec(RedisValue& args) {
    std::cerr << name << " was called.\n";
    if (boost::get<std::vector<RedisValue>>(args).size() == 2)
        if (table_ptr->contains(boost::get<std::vector<char>>(boost::get<std::vector<RedisValue>>(args)[1]))) {
            table_ptr->remove(boost::get<std::vector<char>>(boost::get<std::vector<RedisValue>>(args)[1]));
            return construct_bstr("Value succesfully deleted.");
        } else {
            return RedisError("Value wasn't found.");
        }
    return RedisError("Invalid command use, usage: \"DEL key\" ");
}