#include "writer.h"

void Writer::write_raw(const char* s, size_t len) {
    for(int i = 0; i < len; ) {
        while(i < len && wpos_ != buffer_.size()) {
            buffer_[wpos_++] = s[i++];
        }
    }
}

void Writer::write_char(char c) {
    buffer_[wpos_++] = c;
}

void Writer::write_crlf() {
    write_char('\r');
    write_char('\n');
}

void Writer::write_int(int64_t i) {
    char buff[128];
    int len = snprintf(buff, sizeof(buff), "%ld", i);

    write_raw(buff, len);
}

void Writer::write_string(const std::string& s) {
    write_raw(s.c_str(), s.size());
}

void StringWriter::flush() {
    result.append(buffer_.begin(), buffer_.begin() + wpos_);
    wpos_ = 0;
}

void SocketWriter::flush() {
    socket_ptr->send(&buffer_[0], wpos_);
    wpos_ = 0;
}
