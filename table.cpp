#include "table.h"

Table::Table() {}

bool Table::contains(std::vector<char> key) {
	return data.count(key) != 0;
}

const RedisValue* Table::get(std::vector<char> key) {
	return (contains(key))? &data[key] : NULL;
}

void Table::set(std::vector<char> key, RedisValue& value) {
	if (!contains(key))
		data[key] = RedisValue(value);
}

void Table::remove(std::vector<char> key) {
	if (contains(key))
		data.erase(key);
}
