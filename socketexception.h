#pragma once

#include <exception>
#include <string>

class SocketException: public std::exception {
public:
    SocketException(const char* s): m_s(s) {};
    ~SocketException(){};
    const char* what() const {    return m_s.c_str();    }
private:
    std::string m_s;
}