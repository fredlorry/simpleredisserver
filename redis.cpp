#include "redis.h"

std::vector<char> construct_bstr(const char* cstr) {
    size_t len = 0;
    while (*(++len + cstr));
    return std::vector<char>(cstr, cstr + len);
}

void WriteRedisValue(Writer* w, const RedisValue& value) {
    if (value.which() == REDIS_NULL) {
        w->write_char('$');
        w->write_int(-1);
        w->write_crlf();
    } else if (value.which() == REDIS_INT) {
        w->write_char(':');
        w->write_int(boost::get<int64_t>(value));
        w->write_crlf();
    } else if (value.which() == REDIS_STRING) {
        w->write_char('+');
        w->write_string(boost::get<std::string>(value));
        w->write_crlf();
    } else if (value.which() == REDIS_BULK_STRING) {
        w->write_char('$');
        w->write_int(boost::get<std::vector<char>>(value).size());
        w->write_crlf();
        for (auto i = boost::get<std::vector<char>>(value).begin();
             i != boost::get<std::vector<char>>(value).end();
             ++i)
            w->write_char(*i);
        w->write_crlf();
    } else if (value.which() == REDIS_ERROR) {
        w->write_char('-');
        w->write_string(boost::get<RedisError>(value).msg);
        w->write_crlf();
    } else if (value.which() == REDIS_ARRAY) {
        w->write_char('*');
        
        w->write_int(boost::get<std::vector<RedisValue>>(value).size());
        w->write_crlf();
        for (auto i = 0;
             i != boost::get<std::vector<RedisValue>>(value).size();
             ++i)
            WriteRedisValue(w, boost::get<std::vector<RedisValue>>(value)[i]);
    } else {
        throw std::runtime_error("Unsupported type.");
    }
}

void ReadRedisValue(Reader* r, RedisValue* value) {
    char c;
    switch (c = r->read_char()) {
    case '$': {
        int64_t len = r->read_int();
        if (len > 0) {
            *value = r->read_raw(len);
        } else if (len == 0) {
            r->read_char();
            r->read_char();
            *value = std::vector<char>(0);
        } else {
            *value = RedisNull();
        }
        break;
    }
    case ':': {
        *value = r->read_int();
        break;
    }
    case '+': {
        *value = r->read_line();
        break;
    }
    case '-': {
        *value = RedisError(r->read_line());
        break;
    }
    case '*': {
        size_t size = r->read_int();
        std::vector<RedisValue> v(size);
        for (size_t i = 0; i != size; ++i)
            ReadRedisValue(r, &v[i]);
        *value = v;
        break;
    }
    default: {
        throw std::runtime_error("Invalid Redis value.");
    }
    }
}
