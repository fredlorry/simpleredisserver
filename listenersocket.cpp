#include "listenersocket.h"

ListenerSocket::ListenerSocket(int port) {
    Socket::create();
    Socket::bind(port);
    Socket::listen();
}

ListenerSocket::~ListenerSocket() {}

bool ListenerSocket::accept(Socket& o_sock) {
    return Socket::accept(o_sock);
}
