project(SimpleRedisProject)
cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -std=c++14")

include_directories(.)

add_library(redis
  redis.cpp
  reader.cpp
  writer.cpp
)

add_library(server
  socket.cpp
  listenersocket.cpp
  server.cpp
  cmd.cpp
  table.cpp
)

add_executable(redis-server
  main.cpp
)
target_link_libraries(redis-server server redis pthread)

include_directories(contrib)

add_executable(run_test
  contrib/gmock-gtest-all.cc
  contrib/gmock_main.cc
  test/redis_test.cpp
  test/socket_test.cpp
  )
target_link_libraries(run_test server redis pthread)
