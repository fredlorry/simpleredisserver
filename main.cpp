#include "redis.h"
#include "server.h"

int main(int argc, char *argv[]) {
	close(2);
	if (argc > 1) {
	    std::cout << std::endl << "Log file at /tmp/fakeredis.fakelog" << std::endl;
	    open("/tmp/fakeredis.fakelog", O_WRONLY | O_TRUNC | O_CREAT, 0666);
    } else {
    	open("/dev/null", O_WRONLY, 0666);
    }
    SimpleServer serv;
    serv.start();
    return 0;
}
