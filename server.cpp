#include "server.h"

Server::Server(): state(false), selfsocket(6379), cmds() {}

Server::~Server() {}

void Server::start() {
    state = true;
    std::cerr << "Server started." << std::endl;
    while (state) {
        main_loop();
        sleep(1);
    } // Закончили работу сервера
}

void Server::shutdown() {
    state = false;
}

// SimpleServer

SimpleServer::SimpleServer(): Server(), reader(), writer(), being_idle(0) {
    // add new commands here
    // with ptrs_vec.emplace_back(new $COMMANDNAME(&table, this));
    ptrs_vec.emplace_back(new SHUTDOWN(&table, this));
    ptrs_vec.emplace_back(new SET(&table, this));
    ptrs_vec.emplace_back(new GET(&table, this));
    ptrs_vec.emplace_back(new DEL(&table, this));
    //
    for (size_t i = 0; i != ptrs_vec.size(); ++i)
        cmds.insert(std::make_pair(ptrs_vec[i]->get_name(), std::move(ptrs_vec[i])));
    reader.socket_ptr = &client;
    writer.socket_ptr = &client;
}

void SimpleServer::main_loop() {
    if (client.is_valid()) {
        try {
            RedisValue recv_val;
            ReadRedisValue(&reader, &recv_val);
            RedisValue send_val;
            if (
                recv_val.which() == REDIS_ARRAY
                &&
                boost::get<std::vector<RedisValue>>(recv_val)[0].which() == REDIS_BULK_STRING
                ) {
                std::vector<char> command = boost::get<std::vector<char>>(boost::get<std::vector<RedisValue>>(recv_val)[0]);
                if (cmds.count(command))
                    send_val = cmds[command]->exec(recv_val);
                else
                    send_val = RedisError("Command wasn't found.");
            } else {
                send_val = RedisNull();
            }
            WriteRedisValue(&writer, send_val);
            writer.flush();
        } catch (const std::runtime_error &e) {
            std::cerr << "Something went wrong: " << std::endl;
            std::cerr << "     " << e.what() << std::endl;
            reader.clear();
            client.close();
        }
    } else {
        std::cerr << "Waiting for a client." << std::endl;
        selfsocket.accept(client);
    }
}
