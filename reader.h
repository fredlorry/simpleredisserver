#pragma once

class Reader;

#include <vector>
#include <string>
#include <stdexcept>

#include "socket.h"

class Reader {
public:
    explicit Reader(size_t buffer_size) : buffer_(buffer_size) {}

    void clear();
    
    char read_char(); // read single char
    std::string read_line(); // read until "\r\n" and skip it
    std::vector<char> read_raw(size_t len); // read string with fixed size and skip "\r\n" at the end
    int64_t read_int(); // read integer and skip "\r\n"

protected:
    std::vector<char> buffer_;
    size_t end_ = 0, rpos_ = 0;

    virtual void read_more() = 0;
};

class StringReader : public Reader {
public:
    explicit StringReader(size_t buffer_size = 1024) : Reader(buffer_size) {}

    std::string input;

    virtual void read_more() override;
};

class SocketReader : public Reader {
public:
    explicit SocketReader(size_t buffer_size = MAXRECV + 1, Socket * s_ptr = NULL) : Reader(buffer_size) {
        socket_ptr = s_ptr;
    }

    Socket * socket_ptr;

    virtual void read_more() override;
};
