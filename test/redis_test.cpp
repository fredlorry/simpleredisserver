#include "redis.h"

#include <gtest/gtest.h>

#define GET_INT(val) boost::get<int64_t>(val)
#define GET_ERROR(val) boost::get<RedisError>(val).msg.c_str()
#define GET_STR(val) boost::get<std::string>(val).c_str()
#define GET_ARRAY(val) boost::get<std::vector<RedisValue>>(val)

TEST(RedisValue, Construct) {
    RedisValue null = RedisNull();
    RedisValue integer = 10;
    RedisValue string = "abcd";
    RedisValue b_string = std::vector<char>{'b', '_', 's'};
    RedisValue error = RedisError("Permission denied");
    RedisValue array = std::vector<RedisValue>{integer, string, error};
    EXPECT_EQ(REDIS_NULL, null.which());
    EXPECT_EQ(REDIS_INT, integer.which());
    EXPECT_EQ(REDIS_STRING, string.which());
    EXPECT_EQ(REDIS_BULK_STRING, b_string.which());
    EXPECT_EQ(REDIS_ERROR, error.which());
    EXPECT_EQ(REDIS_ARRAY, array.which());
}



TEST(ReadRedisValue, Int) {
    RedisValue val;
    StringReader reader;

    reader.input = ":10\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(10, GET_INT(val));

    reader.input = ":0\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(0, GET_INT(val));

    reader.input = ":-5\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(-5, GET_INT(val));
}


TEST(ReadRedisValue, Null) {
    RedisValue val;
    StringReader reader;

    reader.input = "$-1\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(REDIS_NULL, val.which());
}


TEST(ReadRedisValue, Error) {
    RedisValue val;
    StringReader reader;

    reader.input = "-Typical Error\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_STREQ("Typical Error", GET_ERROR(val));
}


TEST(ReadRedisValue, Bulk_String) {
    RedisValue val;
    StringReader reader;

    reader.input = "$11\r\nBulk String\r\n";
    ReadRedisValue(&reader, &val);
    std::vector<char> drthck(boost::get<std::vector<char>>(val));
    drthck.push_back('\0');
    EXPECT_STREQ("Bulk String", &drthck[0]);

    reader.input = "$0\r\n\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(0, boost::get<std::vector<char>>(val).size());
}


TEST(ReadRedisValue, String) {
    RedisValue val;
    StringReader reader;

    reader.input = "+Just String\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_STREQ("Just String", boost::get<std::string>(val).c_str());

    reader.input = "+\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(0, boost::get<std::string>(val).size());
}


TEST(ReadRedisValue, Array) {
    RedisValue val;
    StringReader reader;
    
    reader.input = "*6\r\n$-1\r\n:100\r\n+foo\r\n$3\r\nbar\r\n-error\r\n*0\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(REDIS_ARRAY, val.which());
    EXPECT_EQ(REDIS_NULL, GET_ARRAY(val)[0].which());
    EXPECT_EQ(REDIS_INT, GET_ARRAY(val)[1].which());
    EXPECT_EQ(100, GET_INT(GET_ARRAY(val)[1]));
    EXPECT_EQ(REDIS_STRING, GET_ARRAY(val)[2].which());
    EXPECT_EQ(REDIS_BULK_STRING, GET_ARRAY(val)[3].which());
    EXPECT_EQ(REDIS_ERROR, GET_ARRAY(val)[4].which());
    EXPECT_EQ(REDIS_ARRAY, GET_ARRAY(val)[5].which());

    reader.input = "*0\r\n";
    ReadRedisValue(&reader, &val);
    EXPECT_EQ(0, GET_ARRAY(val).size());
    
}



TEST(WriteRedisValue, Int) {
    RedisValue i = 10, j = 0, k = -5;

    StringWriter writer(1024);
    WriteRedisValue(&writer, i);
    writer.flush();
    EXPECT_STREQ(":10\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, j);
    writer.flush();
    EXPECT_STREQ(":0\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, k);
    writer.flush();
    EXPECT_STREQ(":-5\r\n", writer.result.c_str());
}


TEST(WriteRedisValue, Null) {
    RedisValue nil = RedisNull();

    StringWriter writer(1024);
    WriteRedisValue(&writer, nil);
    writer.flush();
    EXPECT_STREQ("$-1\r\n", writer.result.c_str());
}


TEST(WriteRedisValue, Error) {
    RedisValue err = RedisError("Typical Error");

    StringWriter writer(1024);
    WriteRedisValue(&writer, err);
    writer.flush();
    EXPECT_STREQ("-Typical Error\r\n", writer.result.c_str());
}


TEST(WriteRedisValue, Bulk_String) {
    char str[] = "Bulk String";
    RedisValue b_str = std::vector<char>(str, str + sizeof(str)/sizeof(char) - 1);
    RedisValue empt_b_str = std::vector<char>(0);
    
    StringWriter writer(1024);
    WriteRedisValue(&writer, b_str);
    writer.flush();
    EXPECT_STREQ("$11\r\nBulk String\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, empt_b_str);
    writer.flush();
    EXPECT_STREQ("$0\r\n\r\n", writer.result.c_str());
}


TEST(WriteRedisValue, String) {
    RedisValue str = "Just String", empt_str = "";
    StringWriter writer(1024);

    WriteRedisValue(&writer, str);
    writer.flush();
    EXPECT_STREQ("+Just String\r\n", writer.result.c_str());

    writer.result.clear();
    WriteRedisValue(&writer, empt_str);
    writer.flush();
    EXPECT_STREQ("+\r\n", writer.result.c_str());
}


TEST(WriteRedisValue, Array) {
    RedisValue null = RedisNull();
    RedisValue integer = 10;
    RedisValue string = "abcd";
    RedisValue b_string = std::vector<char>{'b', '_', 's'};
    RedisValue error = RedisError("Permission denied.");
    RedisValue array = std::vector<RedisValue>(0);
    std::vector<RedisValue> val = std::vector<RedisValue>{null, integer, string, b_string, error, array};

    StringWriter writer(1024);

    WriteRedisValue(&writer, val);
    writer.flush();
    EXPECT_STREQ("*6\r\n$-1\r\n:10\r\n+abcd\r\n$3\r\nb_s\r\n-Permission denied.\r\n*0\r\n", writer.result.c_str());
}
