#include "socket.h"

#include <gtest/gtest.h>

TEST(SocketClass, Construct) {
    Socket sock;
}



TEST(SocketClass, Create) {
    Socket sock;
    EXPECT_EQ(1, sock.create());
}



TEST(SocketClass, Bind){
    Socket sock;
    sock.create();
    EXPECT_EQ(0, sock.bind(666));
    EXPECT_EQ(1, sock.bind(6379));
}



TEST(SocketClass, Listen) {
    Socket sock;
    sock.create();
    sock.bind(6379);
    EXPECT_EQ(1, sock.listen());
}



TEST(SocketClass, Recv) {
    Socket server, client;
    server.create();
    server.bind(12000);
    server.listen();
    server.set_non_blocking(true);
    client.create();
    client.m_addr.sin_family = AF_INET;
    client.m_addr.sin_port = htons(12000);
    inet_pton(AF_INET, "localhost", &client.m_addr.sin_addr);
    connect(client.m_sock,(sockaddr *) &client.m_addr, sizeof(client.m_addr));
    send(client.m_sock, "HELLO", 5, MSG_NOSIGNAL);
    Socket incoming;
    server.accept(incoming);
    char buf[5];
    try {
        incoming.recv(buf, 5);
    } catch(...) {
        std::cout << buf << std::endl;
    }
    EXPECT_STREQ(buf, "HELLO\b");
}

