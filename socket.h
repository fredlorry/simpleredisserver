#pragma once

class Socket;

#include <stdexcept>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h> 
#include <netdb.h> 
#include <arpa/inet.h>
#include <cstring>
#include <string>

const int MAXCONNECTIONS = 5;
const int MAXRECV = 512;
const int MAXSEND = 512;

class Socket {
public:
    Socket();
    ~Socket();

    bool create();
    bool bind(const int port);
    bool listen() const;
    bool accept(Socket&) const;
    void close();

    bool send(char* buf, size_t len) const;
    int recv(char* buf, size_t len);

    void set_non_blocking(const bool);

    bool is_valid() const {    return m_sock != -1;    }

    int get_fd() const {    return m_sock;    }

//private: COMMENTED FOR TESTING PURPOSES ONLY

    int m_sock;
    sockaddr_in m_addr;

};
