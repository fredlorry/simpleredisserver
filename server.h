#pragma once

class Server;
class SimpleServer;

#include <unordered_map>
#include <vector>
#include <unistd.h>
#include <memory>
#include <sys/types.h>
#include <iostream>

#include "listenersocket.h"
#include "writer.h"
#include "reader.h"
#include "cmd.h"
#include "table.h"
#include "redis.h"


class Server {
public:
    Server();
    ~Server();
    void start();
    void virtual main_loop() {    std::cout << "Wrong server started..";    state = false;   }
    void shutdown();
protected:
    ListenerSocket selfsocket;
    std::vector<std::unique_ptr<Cmd>> ptrs_vec;
    std::unordered_map<std::vector<char>, std::unique_ptr<Cmd>, VCharHash> cmds;
    Table table;
private:
    bool state;
};

// Последовательный сервер.
class SimpleServer: public Server {
public:
    SimpleServer();

    Socket client;
    SocketReader reader;
    SocketWriter writer;
    unsigned being_idle;

    virtual void main_loop() override;
};
