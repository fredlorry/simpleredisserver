#pragma once

#include <unordered_map>
#include "redis.h"

class VCharHash {
public:
    size_t operator() (const std::vector<char> &v) const {
        return std::hash<std::string>()(std::string(v.begin(), v.end()));
    }
};

class Table {
public:
    Table();
    bool contains(std::vector<char>);
    const RedisValue* get(std::vector<char>);
    void set(std::vector<char>, RedisValue&);
    void remove(std::vector<char>);
private:
	std::unordered_map<std::vector<char>, RedisValue, VCharHash> data;
};
